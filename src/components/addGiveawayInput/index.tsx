import React from "react";
import { makeStyles, Theme, createStyles } from "@material-ui/core/styles";
import {
  Paper,
  InputBase,
  Divider,
  IconButton,
  TextField,
  Typography,
} from "@material-ui/core";
import SendIcon from "@material-ui/icons/Send";
import OriginalPostCard from "../originalPostCard";

const AddGiveawayInput = () => {
  const classes = useStyles();
  const [isVisible, setIsVisible] = React.useState(false);

  const clickedButton = () => {
    alert("Call the API!");
    setIsVisible(!isVisible);
  };

  const data = {
    page: "https://www.instagram.com/p/CMKGRXhgKYS/?igshid=1g5be16t6wd0c",
    picture:
      "https://instagram.fnat7-1.fna.fbcdn.net/v/t51.2885-15/e35/s1080x1080/158020850_3826771770751309_5331182390989358394_n.jpg?tp=1&_nc_ht=instagram.fnat7-1.fna.fbcdn.net&_nc_cat=101&_nc_ohc=GtGDCQh3CpAAX_qDMA7&oh=9a2e4d7f90e7f6f9a005dd0366be9f0d&oe=60754896",
    ownerName: "thedressingroomhq",
    ownerAvatar:
      "https://instagram.fnat7-1.fna.fbcdn.net/v/t51.2885-19/s150x150/49616209_359031101553996_6230889170055725056_n.jpg?tp=1&_nc_ht=instagram.fnat7-1.fna.fbcdn.net&_nc_ohc=VdEOETHTlrEAX_Edtaj&oh=92a0316ac395fc69149444042c12949f&oe=60725312",
    allMentions: [
      ["@xsuzanne.hallx", 1],
      ["@missjenchapman", 1],
      ["@clare_91_x", 1],
      ["@jackie_61_x", 1],
      ["@katherinem93", 1],
      ["@kayleighcooper87", 1],
      ["@grace.o.s08", 1],
      ["@busylibbycleans", 1],
      ["@dawn091981", 1],
      ["@nina82x", 1],
      ["@aimzb89", 1],
      ["@_shannonpyke_", 1],
      ["@lashesbymill.x", 1],
      ["@__kj.xx", 1],
      ["@benniondebbie", 1],
      ["@kendalljenner", 1],
      ["@sophiemercer5", 1],
      ["@katiemercer12", 1],
      ["@alice.hopkinsx", 1],
      ["@rey.once", 1],
      ["@keayapowell", 1],
      ["@sophielouise7xx", 1],
      ["@joannewhalleyxx", 1],
      ["@miatyrrell_", 1],
      ["@xtrrjx", 1],
      ["@_kayleigh.moranx", 1],
      ["@darceyjohnsonx", 1],
      ["@cherylgibbx", 1],
      ["@leannebarryfitness", 1],
      ["@hanstephenson_x", 1],
      ["@haylieh15all", 1],
    ],
    totalMentions: 31,
    allTags: [["#klev", 1]],
    totalTags: 1,
    totalComments: 25,
    comments: [
      {
        userName: "thedressingroomhq",
        avatar:
          "https://instagram.fnat7-1.fna.fbcdn.net/v/t51.2885-19/s150x150/49616209_359031101553996_6230889170055725056_n.jpg?tp=1&_nc_ht=instagram.fnat7-1.fna.fbcdn.net&_nc_ohc=VdEOETHTlrEAX_Edtaj&oh=92a0316ac395fc69149444042c12949f&oe=60725312",
        message: 'I1f X7jCj">',
        mentions: [],
        numberMentions: 0,
        tags: [],
        numberTags: 0,
        likes: "not implemented",
      },
      {
        userName: "becki_lincolnx",
        avatar:
          "https://instagram.fnat7-1.fna.fbcdn.net/v/t51.2885-19/s150x150/68949289_377686019547034_8255420630359343104_n.jpg?tp=1&_nc_ht=instagram.fnat7-1.fna.fbcdn.net&_nc_ohc=wII9N0HywwUAX9jRr7K&oh=0232b77f6aa494fa82254eab4562ec1a&oe=6075BAC5",
        message: "@xsuzanne.hallx",
        mentions: [Array],
        numberMentions: 1,
        tags: [],
        numberTags: 0,
        likes: "not implemented",
      },
      {
        userName: "becki_lincolnx",
        avatar:
          "https://instagram.fnat7-1.fna.fbcdn.net/v/t51.2885-19/s150x150/68949289_377686019547034_8255420630359343104_n.jpg?tp=1&_nc_ht=instagram.fnat7-1.fna.fbcdn.net&_nc_ohc=wII9N0HywwUAX9jRr7K&oh=0232b77f6aa494fa82254eab4562ec1a&oe=6075BAC5",
        message: "@missjenchapman",
        mentions: [Array],
        numberMentions: 1,
        tags: [],
        numberTags: 0,
        likes: "not implemented",
      },
      {
        userName: "lauraj_x",
        avatar:
          "https://instagram.fnat7-1.fna.fbcdn.net/v/t51.2885-19/s150x150/157282360_117915660306671_4177080842665903684_n.jpg?tp=1&_nc_ht=instagram.fnat7-1.fna.fbcdn.net&_nc_ohc=6aXG_Bo_XSsAX_HNs7k&oh=5a4b1061ea35af9aa772e5e8d167ecfc&oe=6072A737",
        message: "@clare_91_x @jackie_61_x 👏👏",
        mentions: [Array],
        numberMentions: 2,
        tags: [],
        numberTags: 0,
        likes: "not implemented",
      },
      {
        userName: "danielle_juggles_life",
        avatar:
          "https://instagram.fnat7-1.fna.fbcdn.net/v/t51.2885-19/s150x150/116152465_755134798571225_9215842158245969961_n.jpg?tp=1&_nc_ht=instagram.fnat7-1.fna.fbcdn.net&_nc_ohc=SJeSsCRXnagAX-b6X1d&oh=020c93872c87410f9f94d8ff3297ce5b&oe=6072BFCA",
        message:
          "@katherinem93 @kayleighcooper87 @grace.o.s08 @busylibbycleans",
        mentions: [Array],
        numberMentions: 4,
        tags: [],
        numberTags: 0,
        likes: "not implemented",
      },
      {
        userName: "flynniec",
        avatar:
          "https://instagram.fnat7-1.fna.fbcdn.net/v/t51.2885-19/s150x150/105418571_2767198230230081_8849693567228082912_n.jpg?tp=1&_nc_ht=instagram.fnat7-1.fna.fbcdn.net&_nc_ohc=MjiPC4yrRQwAX_Vi5MB&oh=e90faf07a9ef7308610eaf69b3967b30&oe=607543EC",
        message: "@dawn091981 🤞🏼🤞🏼",
        mentions: [Array],
        numberMentions: 1,
        tags: [],
        numberTags: 0,
        likes: "not implemented",
      },
      {
        userName: "sammydooley16",
        avatar:
          "https://instagram.fnat7-1.fna.fbcdn.net/v/t51.2885-19/s150x150/67552297_2566224933409378_8958411926656778240_n.jpg?tp=1&_nc_ht=instagram.fnat7-1.fna.fbcdn.net&_nc_ohc=LUFeEj6mdj0AX888tC3&oh=8eb8e2028c7234179dd6b46904e392e2&oe=6075244E",
        message: "@nina82x @aimzb89 @_shannonpyke_",
        mentions: [Array],
        numberMentions: 3,
        tags: [],
        numberTags: 0,
        likes: "not implemented",
      },
      {
        userName: "babybella_dummychains",
        avatar:
          "https://instagram.fnat7-1.fna.fbcdn.net/v/t51.2885-19/s150x150/97905059_175431283823355_4680848684248727552_n.jpg?tp=1&_nc_ht=instagram.fnat7-1.fna.fbcdn.net&_nc_ohc=2wzqqKl-bvkAX8ghv5p&oh=ec61b30bc6b890472d07975a6b53930c&oe=60738B1D",
        message: "@lashesbymill.x",
        mentions: [Array],
        numberMentions: 1,
        tags: [],
        numberTags: 0,
        likes: "not implemented",
      },
      {
        userName: "keliseharding_xx",
        avatar:
          "https://instagram.fnat7-1.fna.fbcdn.net/v/t51.2885-19/s150x150/136071071_182930223530259_7048801157244055804_n.jpg?tp=1&_nc_ht=instagram.fnat7-1.fna.fbcdn.net&_nc_ohc=qIyyUuzkxCkAX-wV03D&oh=d67d9766689f53bfca95dc549447e7c6&oe=607427FF",
        message: "💗 @__kj.xx",
        mentions: [Array],
        numberMentions: 1,
        tags: [],
        numberTags: 0,
        likes: "not implemented",
      },
      {
        userName: "xbv7x",
        avatar:
          "https://instagram.fnat7-1.fna.fbcdn.net/v/t51.2885-19/s150x150/83944432_182473759724457_52086301054205952_n.jpg?tp=1&_nc_ht=instagram.fnat7-1.fna.fbcdn.net&_nc_ohc=rICSbOpLhmEAX97VG-z&oh=211ce1e8cd655898756fc4b30a7398bb&oe=60744AF9",
        message: "@benniondebbie",
        mentions: [Array],
        numberMentions: 1,
        tags: [],
        numberTags: 0,
        likes: "not implemented",
      },
      {
        userName: "izabellax.xx",
        avatar:
          "https://instagram.fnat7-1.fna.fbcdn.net/v/t51.2885-19/s150x150/134424763_178408073979997_3882408354435901412_n.jpg?tp=1&_nc_ht=instagram.fnat7-1.fna.fbcdn.net&_nc_ohc=DWcVfG-gaeQAX_ITCvE&oh=8e10038b719033cadfaa8abbcc58576c&oe=60742B1A",
        message: "@kendalljenner inspirational woman🙌🏼",
        mentions: [Array],
        numberMentions: 1,
        tags: [],
        numberTags: 0,
        likes: "not implemented",
      },
      {
        userName: "mercermarion",
        avatar:
          "https://instagram.fnat7-1.fna.fbcdn.net/v/t51.2885-19/s150x150/12907339_1059379647462823_1884893057_a.jpg?tp=1&_nc_ht=instagram.fnat7-1.fna.fbcdn.net&_nc_ohc=4GaVkTrj2bMAX8EIrrF&oh=d913c914027951d2842c39eb0ca8b55d&oe=6073F78C",
        message: "@sophiemercer5 @katiemercer12",
        mentions: [Array],
        numberMentions: 2,
        tags: [],
        numberTags: 0,
        likes: "not implemented",
      },
      {
        userName: "katehough1",
        avatar:
          "https://instagram.fnat7-1.fna.fbcdn.net/v/t51.2885-19/s150x150/121997119_390821981939320_4968470673491231872_n.jpg?tp=1&_nc_ht=instagram.fnat7-1.fna.fbcdn.net&_nc_ohc=ERHxfN0mjXMAX_sb0OZ&oh=f57d2809f5e728d65b3363f4b595aafe&oe=60751B61",
        message: "@alice.hopkinsx @rey.once",
        mentions: [Array],
        numberMentions: 2,
        tags: [],
        numberTags: 0,
        likes: "not implemented",
      },
      {
        userName: "jaquespd",
        avatar:
          "https://instagram.fnat7-1.fna.fbcdn.net/v/t51.2885-19/s150x150/17933784_1137569126366489_3916008767967199232_a.jpg?tp=1&_nc_ht=instagram.fnat7-1.fna.fbcdn.net&_nc_ohc=2f-OcAURNPYAX8ttuae&oh=c6d5b10f0924e703ae514918e90716b2&oe=607430C0",
        message: "#klev",
        mentions: [],
        numberMentions: 0,
        tags: [Array],
        numberTags: 1,
        likes: "not implemented",
      },
      {
        userName: "_kayleigh.moranx",
        avatar:
          "https://instagram.fnat7-1.fna.fbcdn.net/v/t51.2885-19/s150x150/155112339_251804429901344_4367812466216207240_n.jpg?tp=1&_nc_ht=instagram.fnat7-1.fna.fbcdn.net&_nc_ohc=PORK9gsFz3EAX99dzNs&oh=8277fd9a81b4dd8706a91b1d0560e08b&oe=60728976",
        message: "@keayapowell",
        mentions: [Array],
        numberMentions: 1,
        tags: [],
        numberTags: 0,
        likes: "not implemented",
      },
      {
        userName: "_kayleigh.moranx",
        avatar:
          "https://instagram.fnat7-1.fna.fbcdn.net/v/t51.2885-19/s150x150/155112339_251804429901344_4367812466216207240_n.jpg?tp=1&_nc_ht=instagram.fnat7-1.fna.fbcdn.net&_nc_ohc=PORK9gsFz3EAX99dzNs&oh=8277fd9a81b4dd8706a91b1d0560e08b&oe=60728976",
        message: "@sophielouise7xx",
        mentions: [Array],
        numberMentions: 1,
        tags: [],
        numberTags: 0,
        likes: "not implemented",
      },
      {
        userName: "_kayleigh.moranx",
        avatar:
          "https://instagram.fnat7-1.fna.fbcdn.net/v/t51.2885-19/s150x150/155112339_251804429901344_4367812466216207240_n.jpg?tp=1&_nc_ht=instagram.fnat7-1.fna.fbcdn.net&_nc_ohc=PORK9gsFz3EAX99dzNs&oh=8277fd9a81b4dd8706a91b1d0560e08b&oe=60728976",
        message: "@joannewhalleyxx",
        mentions: [Array],
        numberMentions: 1,
        tags: [],
        numberTags: 0,
        likes: "not implemented",
      },
      {
        userName: "_kayleigh.moranx",
        avatar:
          "https://instagram.fnat7-1.fna.fbcdn.net/v/t51.2885-19/s150x150/155112339_251804429901344_4367812466216207240_n.jpg?tp=1&_nc_ht=instagram.fnat7-1.fna.fbcdn.net&_nc_ohc=PORK9gsFz3EAX99dzNs&oh=8277fd9a81b4dd8706a91b1d0560e08b&oe=60728976",
        message: "@miatyrrell_",
        mentions: [Array],
        numberMentions: 1,
        tags: [],
        numberTags: 0,
        likes: "not implemented",
      },
      {
        userName: "_kayleigh.moranx",
        avatar:
          "https://instagram.fnat7-1.fna.fbcdn.net/v/t51.2885-19/s150x150/155112339_251804429901344_4367812466216207240_n.jpg?tp=1&_nc_ht=instagram.fnat7-1.fna.fbcdn.net&_nc_ohc=PORK9gsFz3EAX99dzNs&oh=8277fd9a81b4dd8706a91b1d0560e08b&oe=60728976",
        message: "@xtrrjx",
        mentions: [Array],
        numberMentions: 1,
        tags: [],
        numberTags: 0,
        likes: "not implemented",
      },
      {
        userName: "miatyrrell_",
        avatar:
          "https://instagram.fnat7-1.fna.fbcdn.net/v/t51.2885-19/s150x150/157378869_1115346262301526_5308541003581944200_n.jpg?tp=1&_nc_ht=instagram.fnat7-1.fna.fbcdn.net&_nc_ohc=uv7RfZO6vFQAX_ExPma&oh=f70d7290da4d12a656f7a4603dca80d8&oe=60759000",
        message: "@_kayleigh.moranx",
        mentions: [Array],
        numberMentions: 1,
        tags: [],
        numberTags: 0,
        likes: "not implemented",
      },
      {
        userName: "_kayleigh.moranx",
        avatar:
          "https://instagram.fnat7-1.fna.fbcdn.net/v/t51.2885-19/s150x150/155112339_251804429901344_4367812466216207240_n.jpg?tp=1&_nc_ht=instagram.fnat7-1.fna.fbcdn.net&_nc_ohc=PORK9gsFz3EAX99dzNs&oh=8277fd9a81b4dd8706a91b1d0560e08b&oe=60728976",
        message: "@darceyjohnsonx",
        mentions: [Array],
        numberMentions: 1,
        tags: [],
        numberTags: 0,
        likes: "not implemented",
      },
      {
        userName: "tashawrightt",
        avatar:
          "https://instagram.fnat7-1.fna.fbcdn.net/v/t51.2885-19/s150x150/157044644_270631421103444_7382633882919847589_n.jpg?tp=1&_nc_ht=instagram.fnat7-1.fna.fbcdn.net&_nc_ohc=BkDKR8x9F-IAX_M2aG-&oh=11ce34bb75f2c424efb2fffebf373273&oe=6075BBB9",
        message: "@cherylgibbx 🙌",
        mentions: [Array],
        numberMentions: 1,
        tags: [],
        numberTags: 0,
        likes: "not implemented",
      },
      {
        userName: "xpilkojx",
        avatar:
          "https://instagram.fnat7-1.fna.fbcdn.net/v/t51.2885-19/s150x150/158718726_767762747200473_8962622637790531428_n.jpg?tp=1&_nc_ht=instagram.fnat7-1.fna.fbcdn.net&_nc_ohc=XjNCVsUbVX0AX8ORsx9&oh=581f6a699e5685407820105429ed6c81&oe=6075088D",
        message: "@leannebarryfitness",
        mentions: [Array],
        numberMentions: 1,
        tags: [],
        numberTags: 0,
        likes: "not implemented",
      },
      {
        userName: "xpilkojx",
        avatar:
          "https://instagram.fnat7-1.fna.fbcdn.net/v/t51.2885-19/s150x150/158718726_767762747200473_8962622637790531428_n.jpg?tp=1&_nc_ht=instagram.fnat7-1.fna.fbcdn.net&_nc_ohc=XjNCVsUbVX0AX8ORsx9&oh=581f6a699e5685407820105429ed6c81&oe=6075088D",
        message: "@hanstephenson_x",
        mentions: [Array],
        numberMentions: 1,
        tags: [],
        numberTags: 0,
        likes: "not implemented",
      },
      {
        userName: "xpilkojx",
        avatar:
          "https://instagram.fnat7-1.fna.fbcdn.net/v/t51.2885-19/s150x150/158718726_767762747200473_8962622637790531428_n.jpg?tp=1&_nc_ht=instagram.fnat7-1.fna.fbcdn.net&_nc_ohc=XjNCVsUbVX0AX8ORsx9&oh=581f6a699e5685407820105429ed6c81&oe=6075088D",
        message: "@haylieh15all",
        mentions: [Array],
        numberMentions: 1,
        tags: [],
        numberTags: 0,
        likes: "not implemented",
      },
    ],
    randomComment: {
      userName: "sammydooley16",
      avatar:
        "https://instagram.fnat7-1.fna.fbcdn.net/v/t51.2885-19/s150x150/67552297_2566224933409378_8958411926656778240_n.jpg?tp=1&_nc_ht=instagram.fnat7-1.fna.fbcdn.net&_nc_ohc=LUFeEj6mdj0AX888tC3&oh=8eb8e2028c7234179dd6b46904e392e2&oe=6075244E",
      message: "@nina82x @aimzb89 @_shannonpyke_",
      mentions: ["@nina82x", "@aimzb89", "@_shannonpyke_"],
      numberMentions: 3,
      tags: [],
      numberTags: 0,
      likes: "not implemented",
    },
  };

  return (
    <>
      {!isVisible && (
        <form>
          <TextField
            required
            id="outlined-required"
            label="Username"
            variant="outlined"
            defaultValue=""
            helperText="Instagram Username"
          />{" "}
          <TextField
            required
            id="outlined-password-input"
            label="Password"
            variant="outlined"
            type="password"
            helperText="Instagram Password"
          />
          <div style={{ height: 25 }} />
          <Paper component="form" elevation={3} className={classes.root}>
            <InputBase
              className={classes.input}
              placeholder="Paste the link..."
              inputProps={{ "aria-label": "add giveaway link" }}
            />

            <Divider className={classes.divider} orientation="vertical" />
            <IconButton
              color="primary"
              aria-label="directions"
              onClick={() => clickedButton()}
            >
              <SendIcon />
            </IconButton>
          </Paper>
        </form>
      )}
      {isVisible && <OriginalPostCard data={data} />}
    </>
  );
};

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      padding: "2px 4px",
      display: "flex",
      alignItems: "center",
      width: 400,
    },
    input: {
      marginLeft: theme.spacing(1),
      flex: 1,
    },
    divider: {
      height: 28,
      margin: 4,
    },
  })
);

export default AddGiveawayInput;
