import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Typography from '@material-ui/core/Typography';
import IconButton from "@material-ui/core/IconButton";
import FavoriteIcon from "@material-ui/icons/Favorite";
import RecordVoiceOverIcon from "@material-ui/icons/RecordVoiceOver";
import LocalOfferIcon from "@material-ui/icons/LocalOffer";

const CardWinner = ({ data }) => {
 
  const classes = useStyles();

  return (
    <Card className={classes.root}>
      <CardActionArea>
        <CardMedia
          className={classes.media}
          image={`${data.randomComment.avatar}`}
          title="Contemplative Reptile"
        />
        <CardContent>
          <Typography gutterBottom variant="h5" component="h2">
          {data.randomComment.userName}
          </Typography>
          <Typography variant="body2" color="textSecondary" component="p">
          {data.randomComment.message}
          </Typography>
        </CardContent>
      </CardActionArea>
      <CardActions>
        <IconButton color="primary" aria-label="directions">
          <FavoriteIcon />
          <Typography>1</Typography>
        </IconButton>
        <IconButton color="primary" aria-label="directions">
          <RecordVoiceOverIcon />
          <Typography>{data.randomComment.numberMentions}</Typography>
        </IconButton>
        <IconButton color="primary" aria-label="directions">
          <LocalOfferIcon />
          <Typography>{data.randomComment.numberTags}</Typography>
        </IconButton>
      </CardActions>
    </Card>
  );
};

const useStyles = makeStyles({
  root: {
    maxWidth: 330,
    margin:20
  },
  media: {
    height: 300,
  },
});

export default CardWinner;