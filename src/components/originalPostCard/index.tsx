import React from "react";
import { makeStyles, Theme, createStyles } from "@material-ui/core/styles";
import {
  Card,
  CardHeader,
  CardMedia,
  CardContent,
  CardActions,
  Avatar,
  IconButton,
  Typography,
} from "@material-ui/core";
import { red } from "@material-ui/core/colors";
import FavoriteIcon from "@material-ui/icons/Favorite";
import ShareIcon from "@material-ui/icons/Share";
import LoyaltyIcon from "@material-ui/icons/Loyalty";

const OriginalPostCard = ({ data }) => {
  const classes = useStyles();

  return (
    <Card className={classes.root}>
      <CardHeader
        avatar={
          <Avatar
            aria-label="recipe"
            className={classes.avatar}
            src={data.ownerAvatar}
          >
            {data.ownerName.charAt(0)}
          </Avatar>
        }
        title={data.ownerName}
        subheader="September 14, 2020"
      />
      <CardMedia
        className={classes.media}
        image={data.picture}
        title={data.ownerName}
      />
      <CardContent>
        <Typography variant="body2" color="textSecondary" component="p">
          caption missing from data
        </Typography>
      </CardContent>
      <CardActions disableSpacing>
        <IconButton aria-label="likes">
          <FavoriteIcon />{" "}
          <Typography variant="subtitle1" component="p">
            {data.totalMentions}
          </Typography>
        </IconButton>
        <IconButton aria-label="share">
          <ShareIcon />
          <Typography variant="subtitle1" component="p">
            {data.totalComments}
          </Typography>
        </IconButton>
        <IconButton aria-label="tag">
          <LoyaltyIcon />{" "}
          <Typography variant="subtitle1" component="p">
            {data.totalTags}
          </Typography>
        </IconButton>
      </CardActions>
    </Card>
  );
};

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      maxWidth: 545,
    },
    media: {
      height: 0,
      paddingTop: "76.25%", // 16:9
    },

    avatar: {
      backgroundColor: red[500],
    },
  })
);

export default OriginalPostCard;
