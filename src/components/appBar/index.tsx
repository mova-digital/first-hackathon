import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import { AppBar, Toolbar } from "@material-ui/core";

const CostumAppBar = ({ children }) => {
  
  const classes = useStyles();

  return (
    <div className={classes.grow}>
      <AppBar position="static" color="inherit" className={classes.appBar}>
        <Toolbar>
          <img src="/images/logo.png" className={classes.image} />
        </Toolbar>
      </AppBar>
      <main>{children}</main>
    </div>
  );
};

const useStyles = makeStyles((theme) => ({
  grow: {
    flexGrow: 1,
  },
  appBar: {
    boxShadow: "none",
  },
  image: {
    height:80,
    marginTop:20,
    opacity:0.8
  },
}));

export default CostumAppBar;
