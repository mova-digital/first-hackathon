import React from "react";
import {
  Container,
  Typography,
  Box,
  Grid,
  makeStyles,
} from "@material-ui/core";
import { AddGiveawayInput, ListItem, CostumAppBar } from "components";
import Head from "next/head";
import axios from "axios";

const Index = () => {
  const classes = useStyles();

  return (
    <Container maxWidth="lg">
      <Head>
        <title>Next-Hackathon</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <CostumAppBar />
      <Box my={4} style={{ textAlign: "center" }}>
        <Typography
          variant="h3"
          component="h3"
          color="secondary"
          style={{ fontWeight: "700" }}
          gutterBottom
        >
          pick.win
        </Typography>
        <Grid
          container
          spacing={2}
          direction="column"
          alignItems="center"
          justify="center"
          style={{ minHeight: "10vh" }}
        >
          <Grid item>
            {" "}
            <AddGiveawayInput />
          </Grid>
          <Grid item>
            {" "}
            <ListItem />
          </Grid>
        </Grid>
      </Box>
    </Container>
  );
};

const fetchInfluencers = async () =>
  await axios
    .get("http://localhost:3001/api/influencersList")
    .then((res) => ({
      error: false,
      influencers: res.data,
    }))
    .catch(() => ({
      error: true,
      influencers: null,
    }));

export const getStaticProps = async () => {
  const data = await fetchInfluencers();

  return {
    props: data,
  };
};

const useStyles = makeStyles({});

export default Index;
